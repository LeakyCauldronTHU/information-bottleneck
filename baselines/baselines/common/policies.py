import tensorflow as tf
from baselines.common import tf_util
from baselines.a2c.utils import fc
from baselines.common.distributions import make_pdtype
from baselines.common.input import observation_placeholder, encode_observation
from baselines.common.tf_util import adjust_shape
from baselines.common.mpi_running_mean_std import RunningMeanStd
from baselines.common.models import get_network_builder

import gym
import numpy as np


class PolicyWithValue(object):
    """
    Encapsulates fields and methods for RL policy and value function estimation with shared parameters
    """

    def __init__(self, env, observations, latent, estimate_q=False, vf_latent=None, kl_constraint=None, encoded_x=None,
                 attack_args=None, build_latent=None, latent_compare=None, hidden=None, sess=None, **tensors):
        """
        Parameters:
        ----------
        env             RL environment

        observations    tensorflow placeholder in which the observations will be fed

        latent          latent state from which policy distribution parameters should be inferred

        vf_latent       latent state from which value function should be inferred (if None, then latent is used)

        sess            tensorflow session to run calculations in (if None, default session is used)

        **tensors       tensorflow tensors for additional attributes such as state or mask

        """
        self.latent_compare = latent_compare
        self.hidden = hidden
        self.attack_args = attack_args
        self.build_latent = build_latent
        self.env = env
        self.X = observations
        self.encoded_x = encoded_x
        self.state = tf.constant([])
        self.initial_state = None
        self.__dict__.update(tensors)

        vf_latent = vf_latent if vf_latent is not None else latent

        vf_latent = tf.layers.flatten(vf_latent)
        latent = tf.layers.flatten(latent)
        self.kl_constraint = kl_constraint

        # Based on the action space, will select what probability distribution type
        self.pdtype = make_pdtype(env.action_space)

        self.pd, self.pi = self.pdtype.pdfromlatent(latent, init_scale=0.01)

        # Take an action
        self.action = self.pd.sample()

        # Calculate the neg log of our probability
        self.neglogp = self.pd.neglogp(self.action)
        self.logdist = self.pd.logdist()

        self.sess = sess or tf.get_default_session()

        if estimate_q:
            assert isinstance(env.action_space, gym.spaces.Discrete)
            self.q = fc(vf_latent, 'q', env.action_space.n)
            self.vf = self.q
        else:
            self.vf = fc(vf_latent, 'vf', 1)
            self.vf = self.vf[:,0]

        self.adversaries = self._build_test_attack()

    def get_latents(self, observation):
        latents = []
        for i in range(len(self.latent_compare)):
            latents.append(self._evaluate(self.latent_compare[i], observation)[0])
        return np.array(latents)

    def get_hiddens(self, observation):
        return self._evaluate(self.hidden, observation)[0]

    def _build_test_attack(self):
        attack_step_size = self.attack_args.attack_step_size
        attack_steps = self.attack_args.attack_steps
        epsilon = self.attack_args.epsilon

        encoded_x = tf.identity(self.encoded_x)
        noise = tf.random_normal(self.encoded_x.shape, 0, epsilon / 4)
        noise = tf.clip_by_value(noise, -epsilon / 2, epsilon / 2)
        encoded_x = tf.clip_by_value(encoded_x + noise, 0, 255)
        logdist_static = tf.stop_gradient(tf.log(tf.exp(self.logdist) + 1e-5))

        if self.attack_args.attack_type == 'random':
            adversaries = encoded_x - self.encoded_x
            print("using random noise")
        elif self.attack_args.attack_type in ['policy', 'policy_max', 'policy_min']:
            for h in range(attack_steps):
                policy_latent = self.build_latent(encoded_x)
                policy_dynamic, _ = self.pdtype.pdfromlatent(policy_latent, init_scale=0.01, reuse=tf.AUTO_REUSE)
                logdist_dynamic = policy_dynamic.logdist()

                if self.attack_args.attack_type == 'policy':
                    print("using policy attack")
                    loss = tf.reduce_sum(-tf.reduce_sum(tf.exp(logdist_dynamic) * logdist_static, 1))

                elif self.attack_args.attack_type == 'policy_max':
                    print("using policy_max attack")
                    loss = tf.reduce_sum(-tf.reduce_sum(
                        tf.one_hot(tf.argmax(logdist_static, 1), self.env.action_space.n) * logdist_dynamic, 1))

                elif self.attack_args.attack_type == 'policy_min':
                    print("using policy_min attack")
                    loss = -tf.reduce_sum(-tf.reduce_sum(
                        tf.one_hot(tf.argmin(logdist_static, 1), self.env.action_space.n) * logdist_dynamic, 1))
                else:
                    loss = None
                    print("unauthorized attack type when testing! exit!")
                    exit(0)

                noise = tf.sign(tf.gradients(loss, encoded_x)[0])
                # noise = tf.gradients(loss, encoded_x)[0] * 50000
                encoded_x = encoded_x + attack_step_size * noise
                encoded_x = tf.minimum(tf.maximum(encoded_x, self.encoded_x - epsilon), self.encoded_x + epsilon)
                encoded_x = (tf.clip_by_value(encoded_x, 0, 255))

            adversaries = encoded_x - self.encoded_x
        else:
            adversaries = None

        return adversaries

    def get_adversaries(self, observation, **extra_feed):
        # adversaries = self._evaluate(self.adversaries, observation, **extra_feed)
        adversaries = self._evaluate(self.adversaries, observation, **extra_feed)
        # observation_tmp = np.clip(observation + adversaries, 0, 255)
        # adversaries = observation_tmp - observation
        return adversaries

    def _evaluate(self, variables, observation, **extra_feed):
        sess = self.sess
        feed_dict = {self.X: adjust_shape(self.X, observation)}
        for inpt_name, data in extra_feed.items():
            if inpt_name in self.__dict__.keys():
                inpt = self.__dict__[inpt_name]
                if isinstance(inpt, tf.Tensor) and inpt._op.type == 'Placeholder':
                    feed_dict[inpt] = adjust_shape(inpt, data)

        return sess.run(variables, feed_dict)

    def step(self, observation, **extra_feed):
        """
        Compute next action(s) given the observation(s)

        Parameters:
        ----------

        observation     observation data (either single or a batch)

        **extra_feed    additional data such as state or mask (names of the arguments should match the ones in constructor, see __init__)

        Returns:
        -------
        (action, value estimate, next state, negative log likelihood of the action under current policy parameters) tuple
        """

        a, v, state, neglogp, kl_constraint = self._evaluate([self.action, self.vf, self.state, self.neglogp,
                                                              self.kl_constraint], observation, **extra_feed)
        if state.size == 0:
            state = None
        return a, v, state, neglogp, kl_constraint

    def value(self, ob, *args, **kwargs):
        """
        Compute value estimate(s) given the observation(s)

        Parameters:
        ----------

        observation     observation data (either single or a batch)

        **extra_feed    additional data such as state or mask (names of the arguments should match the ones in constructor, see __init__)

        Returns:
        -------
        value estimate
        """
        return self._evaluate(self.vf, ob, *args, **kwargs)

    def save(self, save_path):
        tf_util.save_state(save_path, sess=self.sess)

    def load(self, load_path):
        tf_util.load_state(load_path, sess=self.sess)


def build_policy(env, policy_network, value_network=None,  normalize_observations=False, estimate_q=False,
                 attack_args=None, **policy_kwargs):
    if isinstance(policy_network, str):
        network_type = policy_network
        policy_network = get_network_builder(network_type)(**policy_kwargs)

    def get_kl_constraint(p, q):
        p_mean, p_std = p
        q_mean, q_std = q
        kl_value = tf.reduce_sum(tf.log(q_std/p_std) + (p_std ** 2 + (p_mean-q_mean) ** 2) / q_std ** 2 / 2 - 0.5, 1)
        return kl_value

    def build_latent(x):
        with tf.variable_scope('pi', reuse=tf.AUTO_REUSE):
            policy_latent_mean, policy_latent_std, hidden = policy_network(x)
            epsilon = [tf.random_normal(policy_latent_mean.get_shape(), 0, 1) for _ in range(20)]
            policy_latent = sum([policy_latent_mean + epsilon[i] * policy_latent_std for i in range(len(epsilon))]) / len(epsilon)
            return policy_latent

    def policy_fn(nbatch=None, nsteps=None, sess=None, observ_placeholder=None):
        ob_space = env.observation_space

        X = observ_placeholder if observ_placeholder is not None else observation_placeholder(ob_space, batch_size=nbatch)

        extra_tensors = {}

        if normalize_observations and X.dtype == tf.float32:
            encoded_x, rms = _normalize_clip_observation(X)
            extra_tensors['rms'] = rms
        else:
            encoded_x = X

        encoded_x = encode_observation(ob_space, encoded_x)

        with tf.variable_scope('pi', reuse=tf.AUTO_REUSE):
            policy_latent_mean, policy_latent_std, hidden = policy_network(encoded_x)

            # if isinstance(policy_latent, tuple):
            #     policy_latent, recurrent_tensors = policy_latent
            #
            #     if recurrent_tensors is not None:
            #         # recurrent architecture, need a few more steps
            #         nenv = nbatch // nsteps
            #         assert nenv > 0, 'Bad input for recurrent policy: batch size {} smaller than nsteps {}'.format(nbatch, nsteps)
            #         policy_latent, recurrent_tensors = policy_network(encoded_x, nenv)
            #         extra_tensors.update(recurrent_tensors)

        _v_net = value_network

        if _v_net is None or _v_net == 'shared':
            vf_latent_mean = policy_latent_mean
            vf_latent_std = policy_latent_std
        else:
            if _v_net == 'copy':
                _v_net = policy_network
            else:
                assert callable(_v_net)

            with tf.variable_scope('vf', reuse=tf.AUTO_REUSE):
                # TODO recurrent architectures are not supported with value_network=copy yet
                vf_latent = _v_net(encoded_x)

        # use shared policy and value hidden representation
        epsilon = [tf.random_normal(policy_latent_mean.get_shape(), 0, 1) for _ in range(20)]
        policy_latent = sum([policy_latent_mean + epsilon[i] * policy_latent_std for i in range(len(epsilon))]) / len(epsilon)

        epsilon_compare = [tf.random_normal(policy_latent_mean.get_shape(), 0, 1) for _ in range(attack_args.samples)]
        # policy_latent_compare = [policy_latent_mean + epsilon_compare[i] * policy_latent_std for i in range(attack_args.samples)]
        # policy_latent_compare = [policy_latent_mean]
        policy_latent_compare = [policy_latent_mean, policy_latent_std]

        vf_latent = vf_latent_mean + epsilon[0] * vf_latent_std
        r_mean, r_std = tf.zeros(policy_latent.get_shape()), tf.ones(policy_latent.get_shape())
        kl_constraint = get_kl_constraint((policy_latent_mean, policy_latent_std), (r_mean, r_std))
        policy = PolicyWithValue(
            env=env,
            observations=X,
            latent=policy_latent,
            vf_latent=vf_latent,
            kl_constraint=kl_constraint,
            encoded_x=encoded_x,
            build_latent=build_latent,
            latent_compare=policy_latent_compare,
            hidden=hidden,
            sess=sess,
            attack_args=attack_args,
            estimate_q=estimate_q,
            **extra_tensors
        )
        return policy

    return policy_fn


def _normalize_clip_observation(x, clip_range=[-5.0, 5.0]):
    rms = RunningMeanStd(shape=x.shape[1:])
    norm_x = tf.clip_by_value((x - rms.mean) / rms.std, min(clip_range), max(clip_range))
    return norm_x, rms

